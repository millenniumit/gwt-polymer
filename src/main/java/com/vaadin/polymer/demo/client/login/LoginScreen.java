package com.vaadin.polymer.demo.client.login;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.vaadin.polymer.elemental.Function;
import com.vaadin.polymer.paper.widget.PaperCard;
import com.vaadin.polymer.paper.widget.PaperTabs;
import com.vaadin.polymer.paper.widget.PaperToast;

/**
 * Copyright(C) 2010 MILLENNIUM IT SOFTWARE (PRIVATE) LIMITED
 * All rights reserved.
 * <p>
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF
 * MILLENNIUM IT SOFTWARE (PRIVATE) LIMITED.
 * <p>
 * This copy of the Source Code is intended for MILLENNIUM IT SOFTWARE (PRIVATE) LIMITED 's internal use only and is
 * intended for view by persons duly authorized by the management of MILLENNIUM IT SOFTWARE (PRIVATE) LIMITED. No
 * part of this file may be reproduced or distributed in any form or by any
 * means without the written approval of the Management of MILLENNIUM IT SOFTWARE (PRIVATE) LIMITED.
 * <p>
 * Created by sameerawit on 6/16/2016.
 */
public class LoginScreen extends Composite {

    interface LoginScreenUiBinder extends UiBinder<HTMLPanel, LoginScreen> {
    }
    private static LoginScreenUiBinder loginScreenUiBinder = GWT.create(LoginScreenUiBinder.class);

    @UiField PaperTabs paperTabs;
    @UiField PaperToast toast;

    public LoginScreen(){
        initWidget(loginScreenUiBinder.createAndBindUi(this));
    }

    @Override
    protected void onLoad() {
        toast.ready(new Function() {
            @Override
            public Object call(Object o) {
                toast.open();
                return null;
            }
        });
    }
}
