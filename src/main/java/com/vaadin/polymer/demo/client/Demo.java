package com.vaadin.polymer.demo.client;

import java.util.Arrays;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.vaadin.polymer.demo.client.login.LoginScreen;
import com.vaadin.polymer.Polymer;
import com.vaadin.polymer.iron.IronFlexLayoutElement;
import com.vaadin.polymer.iron.IronIconElement;
import com.vaadin.polymer.iron.widget.IronFlexLayout;
import com.vaadin.polymer.neon.NeonAnimationBehavior;
import com.vaadin.polymer.paper.PaperIconItemElement;
import com.vaadin.polymer.paper.PaperTabsElement;
import com.vaadin.polymer.paper.PaperToastElement;
import com.vaadin.polymer.vaadin.VaadinIconsElement;
import com.vaadin.polymer.vaadin.widget.VaadinIcons;

public class Demo implements EntryPoint {

    public void onModuleLoad() {
        Polymer.startLoading();

        // Although gwt-polymer-elements takes care of dynamic loading of components
        // when they are created using Polymer.createElement or we use Polymer Widgets,
        // there are certain features which must be loaded previously to start
        // the application. Hence you have to add import tags to your host page or
        // import them dynamically, in this case it might be necessary to wait until
        // the components are ready.

        // The `Polymer` utility class provide a set of methods for facilitating it,
        // you can pass tag-names for standard component locations (tag-name/tag-name.html)
        // or relative urls otherwise.
        Polymer.importHref(Arrays.asList(
                PaperIconItemElement.SRC,
                IronIconElement.SRC,
                VaadinIconsElement.SRC,
                NeonAnimationBehavior.SRC,
                IronFlexLayoutElement.SRC,
                PaperTabsElement.SRC,
                PaperToastElement.SRC
        ));

        Polymer.whenReady(o -> {
            // The app is executed when all imports succeed.
            RootPanel.get().add(new LoginScreen());
            return null;
        });
    }
}
